FROM golang:1.19.0-alpine3.16 AS golang

WORKDIR /go

COPY codeeducation.go . 

RUN go build codeeducation.go

FROM scratch

COPY --from=golang /go/codeeducation .

CMD ["./codeeducation"]